<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\BrandRepository;
use Nette;
use Nette\Application\UI\Form;

/**
 * Trida presenteru, starajici se o znacky
 * Class BrandsPresenter
 * @package App\Presenters
 */
final class BrandsPresenter extends Nette\Application\UI\Presenter
{
    /** @var BrandRepository */
    private $brandRepository;

    /**
     * BrandsPresenter constructor.
     * @param BrandRepository $brandRepository
     */
    public function __construct(BrandRepository $brandRepository)
    {
        parent::__construct();
        $this->brandRepository = $brandRepository;
    }

    /**
     * Funkce vlozi do sablony potrebna data (znacky paginator a parametry).
     * @param int $page
     * @param int $size
     * @param bool $sort
     */
    public function renderDefault(int $page = 1, int $size = 10, bool $sort = true): void
    {
        $paginator = new Nette\Utils\Paginator();
        $paginator->setItemCount($this->brandRepository->getBrandsCount());
        $paginator->setItemsPerPage($size);
        $paginator->setPage($page);
        $brands = $this->brandRepository->getPaginatedBrands($paginator->getLength(), $paginator->getOffset(), $sort);

        $this->template->brands = $brands;
        $this->template->paginator = $paginator;
        $this->template->currentPage = $paginator->getPage();
        $this->template->size = $size;
        $this->template->sort = $sort;
    }

    /**
     * Funkce vytvori formular pro znacku.
     * @return Form
     */
    public function createComponentBrandForm(): Form
    {
        $form = new Form();
        $form->addText('name', 'Značka')
            ->setRequired()->setOption('class', 'col s8');

        $form->addSubmit('submit', 'Přidat')
            ->setHtmlAttribute('class', 'btn blue darken-2');

        $form->onSuccess[] = [$this, 'brandFormSucceeded'];

        return $form;
    }

    /**
     * Funkce vytvori nebo edituje znacku diky datum z formulare.
     * @param Form $form
     * @param array $values
     * @throws Nette\Application\AbortException
     */
    public function brandFormSucceeded(Form $form, array $values)
    {
        $name = $values['name'];
        $brandId = $this->getParameter('brandId');

        try
        {
            if($brandId)
            {
                $this->brandRepository->updateBrand((int) $brandId, $name);
                $this->flashMessage('Značka ' . $name . ' byla úspěšně editována', 'green');
            }
            else
            {
                $this->brandRepository->createNewBrand($name);
                $this->flashMessage('Značka ' . $name . ' byla úspěšně přidána', 'green');
            }

            $this->redirect('Brands:default');
        }
        catch (Nette\Database\UniqueConstraintViolationException $e) //kdyz bude znacka uz existovat
        {
            $form['name']->setHtmlAttribute('class', 'invalid')
                ->addError('tato značka už existuje');
        }
    }

    /**
     * Pri editaci tato funkce vlozi nazev znacky do formulare.
     * @param int $brandId
     * @throws Nette\Application\BadRequestException
     */
    public function actionEdit(int $brandId): void
    {
        $brand = $this->brandRepository->findBrandById($brandId);
        if (!$brand)
        {
            $this->error('Značka nebyla nalezena');
        }

        $this['brandForm']->setDefaults($brand);
    }

    /**
     * Funkce smaze znacku.
     * @throws Nette\Application\AbortException
     * @throws Nette\Application\BadRequestException
     */
    public function actionDelete(): void
    {
        $brandId = $this->getParameter('brandId');

        if($brandId && $this->brandRepository->findBrandById((int) $brandId))
        {
            $this->brandRepository->deleteBrand((int) $brandId);
            $this->flashMessage('Značka s id ' . $brandId . ' byla úspěšně smazána', 'green');
        }
        else
        {
            $this->error('Značka nebyla nalezena');
        }

        $this->redirect('Brands:default');
    }
}
