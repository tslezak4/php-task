<?php


namespace App\Model;

use Nette;

/**
 * Trida, ktera se stara o pristup k databazi k tabulce znacek
 * Class BrandRepository
 * @package App\Model
 */
class BrandRepository
{
    use Nette\SmartObject;

    /** @var Nette\Database\Context */
    private $database;

    /**
     * BrandRepository constructor.
     * @param Nette\Database\Context $database
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Funkce vrati ResultSet znacek serareny dle jmena a s offsetem a limitem pro strankovani.
     * @param int $limit
     * @param int $offset
     * @param bool $sort
     * @return Nette\Database\ResultSet
     */
    public function getPaginatedBrands(int $limit, int $offset, bool $sort): Nette\Database\ResultSet
    {
        return $this->database->query('
			SELECT * FROM brands
			ORDER BY', ['name' => $sort,],
            'LIMIT ?
			OFFSET ?',
            $limit, $offset
        );
        // database explorer by byl lepší, ale musel bych předělat stránkování. Zachovám konzistenci a použiju database core všude
    }

    /**
     * Funkce vrati pocet radku v tabulce znacek.
     * @return int
     */
    public function getBrandsCount(): int
    {
        return $this->database->fetchField('SELECT COUNT(*) FROM brands');
    }

    /**
     * Funkce prida novou znacku do tabulky brands.
     * @param string $name
     * @return Nette\Database\ResultSet
     */
    public function createNewBrand(string $name): Nette\Database\ResultSet
    {
        return $this->database->query('INSERT INTO brands ?', [
            'name' => $name,
        ]);
    }

    /**
     * Funkce najde znacku podle jejiho id.
     * @param int $brandId
     * @return Nette\Database\IRow
     */
    public function findBrandById(int $brandId): Nette\Database\IRow
    {
        return $this->database->fetch('SELECT * FROM brands WHERE id=?', $brandId);
    }

    /**
     * Funkce updatuje existujici znacku podle id.
     * @param int $brandId
     * @param string $name
     * @return Nette\Database\ResultSet
     */
    public function updateBrand(int $brandId, string $name): Nette\Database\ResultSet
    {
        return $this->database->query('UPDATE brands SET ?', [
            'name' => $name,],
        'WHERE id=?', $brandId);
    }

    /**
     * Funkce smaze znacku podle id.
     * @param int $brandId
     */
    public function deleteBrand(int $brandId): void
    {
        $this->database->query('DELETE FROM brands WHERE id=?', $brandId);
    }
}