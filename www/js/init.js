$(document).ready(function(){
    $('.sidenav').sidenav();

    for (let deleteLink of document.getElementsByClassName('delete'))
    {
        let elements = document.getElementsByClassName("delete");
        for(let element of elements) {
            element.onclick = function (event) {
                if(confirm('Opravdu chcete odstranit tuto značku?') === false)
                {
                    event.preventDefault();
                }
            };
        }
    }
});